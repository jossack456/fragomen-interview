﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FragomenInterview.Model;
using fragomen_interview.ApplicationLayer;

namespace FragomenInterview.Controllers
{

    /// <summary>
    /// New Features
    /// Versioning 
    /// More to discuss here on tomorrow
    /// </summary>
    [Route("api/v1/[controller]")]
    public class QuotesController : ControllerBase
    {
        private readonly IQuoteService quoteService;
        public QuotesController(IQuoteService _quoteService)
        {
            quoteService = _quoteService;
        }
        // GET api/quotes
        [HttpGet]
        public  IActionResult Get()
        {
              var  response = quoteService.GetAllasycAsync();
            if(response != null)
            {
                return Ok(response);
            }
            return NotFound();
        }
        //Get api/v1/quotes/id/
        [HttpGet("{Input}/{Unique}/{QualifyQuotes}")]
        public ActionResult GetUniquePairsOfQuoteAsync(int Input)
        {
            var response = quoteService.GetUniqueCombination(Input);
            if (response != null)          
                return Ok(response);           
            return Content("No quote Found ");
        }
        // GET api/quotes/5
        [HttpGet("{id}")]
        public IActionResult GetQuotebyIdAnsync(int id)
        {
            QuoteDTO quote = quoteService.FindById(id);
            if(quote != null)          
                return Ok(quote);            
            return Content("No Quote Found for that Id");
        }

        // POST api/quotes
        [HttpPost]
        public IActionResult Post([FromBody] QuoteDTO quote)
        {
            var response = quoteService.Add(quote);
            return Ok(response);
        }

        // PUT api/quotes/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] QuoteDTO quote)
        {
            return Ok(quoteService.Update(quote, id));
        }

        // DELETE api/quotes/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            //example  of a best practice or what i call and I like to see my controller to look like aka "Skinny controller"
            //Will discuss more
            return Ok(quoteService.Delete(id));
        }     
    }
}
