﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fragomen_interview.ApplicationLayer;
using fragomen_interview.DomainLayer;
using fragomen_interview.InfrastructureLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FragomenInterview
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowEverythings", builder => { builder.AllowAnyOrigin(); }));
            services.AddTransient<IQuoteService, QuoteService>();
        
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //Added and configure loggerfactory
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

           loggerFactory.AddConsole(Configuration.GetSection("Logging")); 
            loggerFactory.AddDebug();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
