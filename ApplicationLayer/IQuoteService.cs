using fragomen_interview.DomainLayer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace fragomen_interview.ApplicationLayer
{
    public interface IQuoteService
    {
     
        QuoteDTO FindById(int key);
        ICollection<QuoteDTO> GetAllasycAsync();
        string Delete(long key);
        string Update(QuoteDTO entity, int key);
        string Add(QuoteDTO enity);
        UniqueCombination GetUniqueCombination(int input);
    }
}