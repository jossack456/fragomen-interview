using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using fragomen_interview.DomainLayer;

namespace fragomen_interview.ApplicationLayer.Factory
{
    public class DTOFactory
    {
        public static List<QuoteDTO> CreateUniquePairQuoteDtoModel(List<Quote> ListofQuotes)
        {
            List<QuoteDTO> quotes = new List<QuoteDTO>();
            foreach (var list in ListofQuotes)
            {
                quotes.Add(new QuoteDTO(list.Id, list.Name, list.Text));
            }
            return quotes;
        }
        public static ICollection<QuoteDTO> CreateQuoteDto(ICollection<Quote> ListofQuotes)
        {
            List<QuoteDTO> quotes = new List<QuoteDTO>();
             foreach(var list in ListofQuotes)
            {
                quotes.Add(new QuoteDTO( list.Id,  list.Name,  list.Text));
            }
            return quotes;         
        }
        internal static QuoteDTO CreateQuoteDto(Quote quote)
        {
         
            return new QuoteDTO()
            {            
                Id = quote.Id, Text = quote.Text, Name = quote.Name            
            };
        }

        internal static Quote CreateQuoteDto(QuoteDTO entity)
        {
            return new Quote()
            {
                Id = entity.Id,
                Name = entity.Name,
                Text = entity.Text
            };
        }
    }
}