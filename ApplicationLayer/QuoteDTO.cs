using System;
using System.Collections.Generic;
using System.Text;
using fragomen_interview.DomainLayer;

namespace fragomen_interview.ApplicationLayer
{
    public class QuoteDTO

    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string StatusResponse { get; set; }
        public QuoteDTO(int id, string Name, string text)
        {
            this.Name = Name;
            this.Id = id;
            this.Text = text;
            //this.StatusResponse = statusresonse;
        }

        public QuoteDTO()
        {

        }
    }
    public class UniqueCombination
    {
        public int TotalUniqueCombinationCounts { get; set; }
        public int NumberOfQualifyQuotes { get; set; }
        public int Input { get; set; }
        public UniqueCombination(int totaluniquecombination, int uniquepairs)
        {
            this.TotalUniqueCombinationCounts = totaluniquecombination;
            this.NumberOfQualifyQuotes = uniquepairs;
        }

        public UniqueCombination()
        {
        }

    

    }
}