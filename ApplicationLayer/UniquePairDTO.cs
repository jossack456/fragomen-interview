﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FragomenInterview.ApplicationLayer
{
    public class UniquePairDTO
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }

        public int TotalDistinctRecord { get; set; }

        public UniquePairDTO(int id, string author, string text, int totalDistinctRecord)
        {
            this.Author = author;
            this.Id = id;
            this.Text = text;

        }
    }
}
