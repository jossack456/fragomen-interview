using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using fragomen_interview.ApplicationLayer.Factory;
using fragomen_interview.DomainLayer;
using FragomenInterview.ApplicationLayer.Extensions;
using Newtonsoft.Json;

namespace fragomen_interview.ApplicationLayer
{
    public class QuoteService : IQuoteService
    {
        private readonly  IRepository<Quote> quoteRepository;

        public QuoteService(IRepository<Quote> _quoteRepository)
        {
            quoteRepository = _quoteRepository;
        }

        public string  Add(QuoteDTO enity)
        {
            Quote quote = DTOFactory.CreateQuoteDto(enity);
            var result = quoteRepository.Add(quote);
            if (result == 0) return "Error";
            return "Success";
        }

        public string Delete(long key)
        {
            bool Isdeleted = quoteRepository.Delete(key);
            if (Isdeleted) return "Quote was successfully deleted for that author";
            return "Quote wasn't deleted,Error";
        }

        public QuoteDTO FindById(int key)
        {
            Quote quote = quoteRepository.FindById(key);
            if (quote == null) return null;
            return DTOFactory.CreateQuoteDto(quote);
        }

        public ICollection<QuoteDTO> GetAllasycAsync()
        {
            var  result =   quoteRepository.GetCollectionsOfQuotes();
            return DTOFactory.CreateQuoteDto(result);
        }
        //Could have return collecitons but since this is just a demo, 
        // keep it simle
        public UniqueCombination GetUniqueCombination(int input)
        {
            //get fresh data from json file as list
            var data = quoteRepository.GetListOfQuotes();   
             //copy data to string array
            string[] arr = new string[data.Count];
            data.CopyTo(arr);
            //get all possible combination
            var combination = ReturnUniquePairs.GenerateUniquePairs(arr);
            //return  unique pair
            var result = ReturnUniquePairs.CreateUniquePairsofcombination(combination, input);
            return result;
        }

        public string Update(QuoteDTO entity, int key)
        {
            Quote quote = DTOFactory.CreateQuoteDto(entity);
          var response =  quoteRepository.Update(quote, key);
            if (response == "Success") return "Updated Successfully";
            return "Error";
        }
    }
}