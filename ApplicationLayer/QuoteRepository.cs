using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using fragomen_interview.DomainLayer;

namespace fragomen_interview.ApplicationLayer
{
    public class QuoteRepository 
    {
        public Task<int> CountAsync()
        {
            throw new NotImplementedException();
        }

        public void DeleteAsyn(Quote entity)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Quote>> FindAllAsync(Expression<Func<Quote, bool>> match)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Quote> FindById(Expression<Func<Quote, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<Quote> FindById(int Id)
        {
            throw new NotImplementedException();
        }

        public Quote Get(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Quote> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<int> SaveAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Quote> UpdateAsyn(Quote t, object key)
        {
            throw new NotImplementedException();
        }

      
    }
}