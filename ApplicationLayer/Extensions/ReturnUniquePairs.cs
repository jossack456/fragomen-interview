﻿using fragomen_interview.ApplicationLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FragomenInterview.ApplicationLayer.Extensions
{
    public  class ReturnUniquePairs
    {
        public static List<List<string>> GenerateUniquePairs(string[] arr)
        {
            List<List<string>> combinations = new List<List<string>>();
            int length = arr.Length;

            for (int i = 0; i < (1 << length); ++i)
            {
                List<string> combination = new List<string>();
                int count = 0;

                for (count = 0; count < length; ++count)
                {
                    if ((i & 1 << count) > 0)
                        combination.Add(arr[count]);
                }

                if (count > 0 && combination.Count > 0)
                {
                    var test = combination.Count();
                    combinations.Add(combination);
                }
            }
            return combinations;
        }
        public static UniqueCombination CreateUniquePairsofcombination(List<List<string>> possiblecombination, int input)
        {
            UniqueCombination combination = new UniqueCombination();
            foreach (var lists in possiblecombination)
            {
                if (lists.Count == 2)
                {
                    StringBuilder stringbb = new StringBuilder();
                    foreach (var newlist in lists)
                    {
                        stringbb.Append(newlist);
                    }
                    if (stringbb.Length <= input)
                    {

                        var value = stringbb.Length;
                        combination.NumberOfQualifyQuotes++;

                    }
                }


            }

            combination.TotalUniqueCombinationCounts = possiblecombination.Distinct().Count();
            combination.Input = input;
            return combination;
        }
    }
}
