using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using fragomen_interview.DomainLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fragomen_interview.InfrastructureLayer
{
    public  class Repository<T> : IRepository<T> where T : class
    {
         private const string path = @"ShortDb.json";
        protected static ICollection<Quote> GetQuotes ;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        public ICollection<Quote> GetCollectionsOfQuotes()
        {
            List<Quote> jsonDataList = new List<Quote>();
            if (GetQuotes == null)
            {
                //if (!File.Exists(@"/ShortDb.json"))
                if (!File.Exists(@"/ShortDb.json"))
                    try
                    {
                        using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        using (BufferedStream bs = new BufferedStream(fs))
                        using (StreamReader sr = new StreamReader(bs))
                        {
                            string line;
                            while ((line = sr.ReadLine()) != null)
                            {
                              GetQuotes = JsonConvert.DeserializeObject<ICollection<Quote>>(line);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;
                    }
            }          
                return GetQuotes;         
        }
        public List<string> GetListOfQuotes()
        {
            List<string> jsonDataList = new List<string>();
      
        
                //if (!File.Exists(@"/ShortDb.json"))
                if (!File.Exists(@"/ShortDb.json"))
                    try
                    {
                    List<Quote> getlistofjson = new List<Quote>();
                    using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        //Since am getting a large file ,Buffer stream comes in handy because its reduce the number of calls
                        //I need c
                        using (BufferedStream bs = new BufferedStream(fs))
                            //transfering my mydata 
                        using (StreamReader sr = new StreamReader(bs))
                        {
                            string line;
                            while ((line = sr.ReadLine()) != null)
                            {
                            //jsonDataList = JsonConvert.DeserializeObject<List<string>>(line);
                            getlistofjson = JsonConvert.DeserializeObject<List<Quote>>(line);                         
                            }
                           // jsonDataList.AddRange(line);
                        }

                        foreach(var data in getlistofjson)
                        {
                            jsonDataList.Add(data.Text);
                        }

                    }
                    catch (Exception ex)
                    {
                        var error = ex.Message;
                    }
            
            return jsonDataList;
        }
        public Quote FindById(int Id)
        {
            return GetCollectionsOfQuotes().FirstOrDefault(x =>x.Id == Id);
        }

        public bool Delete(long id)
        {
            try
            {
                var contents = GetCollectionsOfQuotes();
                File.WriteAllText(path, JsonConvert.SerializeObject(contents.Select(x => x.Id != id)));
                return true;
            }
            catch(Exception ex)
            {
                //logger error or exception

            }
            return false;
        }

        public  string  Update(Quote quote, int  key)
        {
            if (key < 0) return null;
            var  exist = GetCollectionsOfQuotes().FirstOrDefault(k => k.Id == key);
            if(exist != null)
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(quote.Id);
                    sw.WriteLine(quote.Name);
                    sw.WriteLine(quote.Text);
                }
                return "Success";
                //Add function logic to update file to save to original location  if getcollectionofquotes return inmemory contents
            }
            return "Error";
        }

        public int Add(Quote enity)
        {
            var contents = GetCollectionsOfQuotes();
            try
            {
                enity.Id = contents.Any() ? contents.Select(x => x.Id).Max() + 1 : 0;
                File.WriteAllText(path, JsonConvert.SerializeObject(contents));
                return enity.Id;
                //Add function logic to update file to save to original location  if getcollectionofquotes return inmemory contents
            }
            catch (Exception ex)
            {
                //logger error here 
            }

            return 0;
        }

     
    }

}