using System;
using System.Collections.Generic;
using System.Linq;
 
namespace FragomenInterview.Model
{
    public class Quote
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}