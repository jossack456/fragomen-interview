namespace fragomen_interview.DomainLayer
{
    public class Quote  
    {
       public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}