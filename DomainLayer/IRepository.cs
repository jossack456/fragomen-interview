using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace fragomen_interview.DomainLayer
{
    public interface IRepository<T>  where T : class  
    {

        //Task<ICollection<T>> FindAllAsync(System.Linq.Expressions.Expression<Func<T, bool>> match);

        // IQueryable<T> FindById(Expression<Func<T, bool>> predicate);
        //// T Update(T t, object key);
        // Task<ICollection<Quote>> GetAllAsyn();
        // Task<T> UpdateAsyn(T t, object key);
        // //void Delete(T entity);
        // void DeleteAsyn(T entity);
        ////  void Save();
        // Task<int> SaveAsync();
        //  T Get(int id);
        //  Task<int> CountAsync();
        ICollection<Quote> GetCollectionsOfQuotes();
        Quote FindById(int key);
        bool  Delete( long key);
        string  Update(Quote entity,int  key);
        int Add(Quote enity);
        List<string> GetListOfQuotes();
        //I need to create a new func to get new pairs of data from the json file
        //So correctly the getallcollectionofquote makes one call and store the in memory 
        //So if that data has been modify(in memory) and not committed which i will talk about in the interview
        //then  the result set might not match the example i was given on the instructions 
        //for the sake of time , i won't implement it now. 

    }
}