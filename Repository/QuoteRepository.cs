using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using FragomenInterview.Model;
using Newtonsoft.Json;
 
namespace FragomenInterview.Repository
{
    public class QuoteRepository
    {
        private const string path = "ShortDb.json";

        public ICollection<Quote> GetAll() 
        {
            if (!File.Exists(path))
                return new List<Quote>();
            var contents = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<ICollection<Quote>>(contents);
        }

        public Quote GetById(long id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public long Create(Quote q) 
        {
            var contents = GetAll();
            q.Id = contents.Any() ? contents.Select(x=>x.Id).Max() + 1 : 0;
            contents.Add(q);
            File.WriteAllText(path, JsonConvert.SerializeObject(contents));
            return q.Id;
        }
        //This doesn't look right , would like to discuss in the code review
        public void Update(long id, Quote q)
        {
            var contents = GetAll();
            var found = contents.FirstOrDefault(x => x.Id == id);
            if (found == null)
                return;
            found.Name = q.Name;
            found.Text = q.Text;
            File.WriteAllText(path, JsonConvert.SerializeObject(contents));
        }

        public void Delete(long id)
        {
            var contents = GetAll();
            File.WriteAllText(path, JsonConvert.SerializeObject(contents.Select(x => x.Id != id)));            
        }
    }
}